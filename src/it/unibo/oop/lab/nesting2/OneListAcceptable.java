package it.unibo.oop.lab.nesting2;

import java.util.ArrayList;
import java.util.List;

public class OneListAcceptable<T> implements Acceptable<T> {

	private List<T> elemList;
	
	public OneListAcceptable(List<T> elemList) {
		this.elemList = new ArrayList<>(elemList); 		
	}
	
	@Override
	public Acceptor<T> acceptor() {
		return new ListAcceptor();
	}
	
	private class ListAcceptor implements Acceptor<T>{
		
		private int index;
		
		public ListAcceptor() {
			this.index = 0;
		}
		
		@Override
		public void accept(T newElement) throws ElementNotAcceptedException {
			if (this.index >= OneListAcceptable.this.elemList.size()
					|| !OneListAcceptable.this.elemList.get(this.index).equals(newElement)) {
				throw new ElementNotAcceptedException(newElement);
			} else {
				this.index = this.index + 1;
			}
		}

		@Override
		public void end() throws EndNotAcceptedException {
			if(OneListAcceptable.this.elemList.size()!=this.index) {
				throw new EndNotAcceptedException();
			}
			
		}
		
	}

}
